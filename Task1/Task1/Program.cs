﻿using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите год:");

            int year = int.Parse(Console.ReadLine());

            string leapYear = year % 4 == 0 ? "это високосный год" : "это не високосный год";

            Console.WriteLine($" {year} {leapYear} ");

            Console.ReadKey();
        }
    }
}
