﻿using System;

namespace task_3
{
    class Program
    {
        static void Main(string[] args)
        {
            double firstCoef, secondCoef, thirdCoef;

            Console.WriteLine("Введите 3 коэффицкнта уравнения:");

            while (!double.TryParse(Console.ReadLine(), out firstCoef) || !double.TryParse(Console.ReadLine(), out secondCoef) || !double.TryParse(Console.ReadLine(), out thirdCoef))
            {
                Console.WriteLine("Введены некорректные данные. Попробуйте снова:");
            }

            double descriminant = Math.Pow(secondCoef, 2) - 4 * firstCoef * thirdCoef;

            if (descriminant > 0)
            {
                double firstRoot = (-secondCoef + Math.Sqrt(descriminant)) / 2.0 / firstCoef;
                double secondRoot = (-secondCoef - Math.Sqrt(descriminant)) / 2.0 / firstCoef;
                Console.WriteLine($"Первый корень: {firstRoot} Второй корень: {secondRoot}");
            }
             else if (descriminant < 0)
            {
                Console.WriteLine("Корней нет");
            }

            else
            {
                double root = -secondCoef / 2.0 / firstCoef;
                Console.WriteLine($"Единственный корень равен: {root}");
            }
            Console.ReadKey();
        }

    }
}
