﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int firstNumber, secondNumber, thirdNumber;

            Console.WriteLine("Введите 3 числа:");

            while (!int.TryParse(Console.ReadLine(), out firstNumber) || 
            !int.TryParse(Console.ReadLine(), out secondNumber) || 
            !int.TryParse(Console.ReadLine(), out thirdNumber)) ;
            {
                Console.WriteLine("Введены неверные данные. Попробуйте снова:");
            }

            int maxNumber = Math.Max(firstNumber, Math.Max(secondNumber, thirdNumber));

            int minNumber = Math.Min(firstNumber, Math.Min(secondNumber, thirdNumber));

            int avgNumber;

            if (firstNumber > secondNumber && firstNumber < thirdNumber || firstNumber < secondNumber && firstNumber > thirdNumber)
            {
                avgNumber = firstNumber;
            }

            else if (secondNumber > firstNumber && secondNumber < thirdNumber || secondNumber < firstNumber && secondNumber > thirdNumber)

            {
                avgNumber = secondNumber;
            }

            else
            {
                avgNumber = thirdNumber;
            }

            Console.WriteLine($"В порядке убывания: {maxNumber}, {avgNumber}, {minNumber}");

            Console.WriteLine($"В порядке возрастания: {minNumber}, {avgNumber}, {maxNumber}");

            Console.WriteLine($"Минимальное значение: {minNumber} \nМаксимальное значение: {maxNumber} \nСреднее значение: {avgNumber}");

            Console.ReadKey();
          

        }
    }
}
